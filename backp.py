# import fileinput
import fileinput
from os import stat
  
# Using fileinput.input() method

LETTER = 0
DIGIT = 1
UNKNOWN = 99
INT_LIT = 10
IDENT = 11
SEMI_COLON = 69
ASSIGN_OP = 20
ADD_OP = 21
SUB_OP = 22
MULT_OP = 23
DIV_OP = 24
LEFT_PAREN = 25
RIGHT_PAREN = 26
FOR_CODE = 30
IF_CODE = 31
WHILE_CODE = 32
INT_CODE=  35
FLOAT_CODE = 36
ELSE_CODE = 37
SWITCH_CODE = 38
DO_CODE = 39
LEFT_BRACKET = 88
RIGHT_BRACKET = 99
VOID = 54
DO_WHILE = 55
RETURN = 56
FOR_EACH = 57
MAIN = 59
CASE_CODE = 60
SWITCHDEFAULT = 61

myCodeTokens = []
allOfMyTokens = []
numberArray = {"0" , "1" , "2" , "3", "4", "5", "6", "7", "8", "9", "."}
letterArray = {"a" , "b" , "c" , "d" , "e" , "f", "g" , "h" , "i" , "j" , "k" , "l" , "m" , "n" , "o" , "p", "q", "r" , "s" , "t" , "u", "v" , "w" , "x" , "y" , "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
specialThings = ["public","int", "main", "String", "static", "String[]", "do", "while", "float", "int", "if", "else", "switch", ";", "void"]
lpar = 0
rpar = 0
lbrack = 0
rbrack = 0

def lex(lpar, rpar, lbrack, rbrack):
    try:
        file = open('lex.txt', 'r')
    except (FileNotFoundError):
        print("Cannot open test file");
    check = 0
    while check < 1000:
        # read by character
        check += 1
        char = file.read(1)  
        if char == "{":
            print("Next token is: " + str(LEFT_BRACKET) + " Next lexeme is " + char);
            print("<ENTER BLOCK>")
            lbrack +=1
            allOfMyTokens.append(LEFT_BRACKET);
        if char == "}":
            print("Next token is: " + str(RIGHT_BRACKET) + " Next lexeme is " + char);
            print("<EXIT BLOCK>")
            rbrack +=1
            allOfMyTokens.append(RIGHT_BRACKET);
        if char == "+":
            print("Next token is: " + str(ADD_OP) + " Next lexeme is " + char);
            allOfMyTokens.append(ADD_OP);
        if char == "-":
            print("Next token is: " + str(SUB_OP) + " Next lexeme is " + char);
            allOfMyTokens.append(SUB_OP);
        if char == "(":
            print("Next token is: " + str(LEFT_PAREN) + "Next lexeme is " + char);
            print("<ENTER EXPRESSION>")
            lpar += 1
            allOfMyTokens.append(LEFT_PAREN);
        if char == ")":
            print("Next token is: " + str(RIGHT_PAREN) + "Next lexeme is " + char);
            print("<EXIT EXPRESSION>")
            rpar +=1
            allOfMyTokens.append(RIGHT_PAREN);
        if char == "*":
            print("Next token is: " + str(MULT_OP) + " Next lexeme is " + char);
            allOfMyTokens.append(MULT_OP);
        if char == "/":
            print("Next token is: " + str(DIV_OP) + " Next lexeme is " + char);
            allOfMyTokens.append(DIV_OP);
        if char == "=":
            print("Next token is: " + str(ASSIGN_OP) + " Next lexeme is " + char);
            allOfMyTokens.append(ASSIGN_OP);
        if char == ";":
            print("Next token is: " + str(SEMI_COLON) + " Next lexeme is " + char);
            allOfMyTokens.append(SEMI_COLON);
            print("")
        if char in numberArray:
            print("Next token is: " + str(INT_LIT) + " Next lexeme is " + char, end="", flush=True);
            allOfMyTokens.append(INT_LIT);
            while char in numberArray:
                char = file.read(1)
                print (char, end="", flush=True);
            print("")
        
        if char in letterArray:
            myString = char;
            while char in letterArray:
                char = file.read(1)
                myString += char;
                if myString in specialThings:
                    char = file.read(1);
                    if char in letterArray:
                         myString += char
                    else:
                        if myString == "void":
                            print("Next token is: " + str(VOID) + " Next lexeme is " + myString);
                            allOfMyTokens.append(VOID);
                            myCodeTokens.append(myString);
                        if myString == "main":
                            print("Next token is: " + str(MAIN) + " Next lexeme is " + myString);
                            allOfMyTokens.append(MAIN);
                            myCodeTokens.append(myString);
                        if myString == "do":
                            print("Next token is: " + str(DO_CODE) + " Next lexeme is " + myString);
                            allOfMyTokens.append(DO_CODE);
                            myCodeTokens.append(myString);
                        if myString == "while":
                            print("Next token is: " + str(WHILE_CODE) + " Next lexeme is " + myString);
                            allOfMyTokens.append(WHILE_CODE);
                            myCodeTokens.append(myString);
                        if myString == "do-while":
                            print("Next token is: " + str(DO_WHILE) + " Next lexeme is " + myString);
                            allOfMyTokens.append(DO_WHILE);
                            myCodeTokens.append(myString);
                        if myString == "FOR_EACH":
                            print("Next token is: " + str(FOR_EACH) + " Next lexeme is " + myString);
                            allOfMyTokens.append(FOR_EACH);
                            myCodeTokens.append(myString);
                        if myString == "float":
                            print("Next token is: " + str(FLOAT_CODE) + " Next lexeme is " + myString);
                            allOfMyTokens.append(FLOAT_CODE);
                            myCodeTokens.append(myString);
                        if myString == "int":
                            print("Next token is: " + str(INT_CODE) + " Next lexeme is " + myString);
                            allOfMyTokens.append(INT_CODE);
                            myCodeTokens.append(myString);
                        if myString == "if":
                            print("Next token is: " + str(IF_CODE) + " Next lexeme is " + myString);
                            allOfMyTokens.append(IF_CODE)
                            myCodeTokens.append(myString)
                        if myString == "else":
                            print("Next token is: " + str(ELSE_CODE) + " Next lexeme is " + myString);
                            allOfMyTokens.append(ELSE_CODE)
                            myCodeTokens.append(myString)
                        if myString == "switch":
                            print("Next token is: " + str(SWITCH_CODE) + " Next lexeme is " + myString);
                            allOfMyTokens.append(SWITCH_CODE)
                            myCodeTokens.append(myString);   
                if char == " ":
                    allOfMyTokens.append(IDENT)
                    print("Next token is: " + str(IDENT) + " Next lexeme is " + myString)
    print("Next token is: -1" + " Next lexeme is EOF")
    if VOID not in allOfMyTokens:
        print("ERROR NO RETURN TYPE")
    if MAIN not in allOfMyTokens:
        print("ERROR NO MAIN METHOD")
    if lpar != rpar:
        print("Error in Paranthesis")
    if lbrack != rbrack:
        print("Error in Brackets")
    return allOfMyTokens
lex(lpar, rpar, lbrack, rbrack)

def syn(allOfMyTokens):
    length = len(allOfMyTokens)
    y = allOfMyTokens
    i = 0
    if INT_LIT in y:
            while i < length - 1:
                if allOfMyTokens[i] == INT_LIT and allOfMyTokens[i + 1] == ADD_OP or SUB_OP or DIV_OP or MULT_OP:
                    if (i+1 > length) or (i+1 < length) or (i+2 > length) or (i+3 > length):
                        #print("Math Syntax is not valid for expression")
                        i = i + 1
                        return
                else:
                    print("Math Syntax is not valid for expression")
                i = i + 1
                return
    if SWITCH_CODE in y:
        if CASE_CODE in y:
            if SWITCHDEFAULT in y:
                return
        else: ("Error switch code does not have required cases or default statement")
    
        
    return

syn(allOfMyTokens)



# <if_statement> -> if( <boolexpr> ) <statement> [else <statement>]
# <for_loop> -> for '(' [expression] ';' [expression] ':' [expression] ')' <statement>
# <while_loop> while '(' [expression] ')' <statement>
# Analyzer for java



